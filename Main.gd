extends Node


var level = 1


func set_level(lvl) -> void:
	level = lvl
	$Level.level = lvl
	$Menu.level = lvl

func _ready() -> void:
	set_level(level)
	$Menu.show()
	$Level.hide()

	var _set_level = $Menu.connect("set_level", self, "_on_Menu_set_level")
	var _close_level = $Level.connect("close_level", self, "_on_Level_close_level")



func _on_Menu_set_level(lvl) -> void:
	set_level(lvl)
	$Menu.hide()
	$Level.show()
	$Level.load_level(lvl) # загружаем в фоне первый уровень
	$Level.scan_board()


func _on_Level_close_level() -> void:
	$Menu.show()
	$Level.hide()