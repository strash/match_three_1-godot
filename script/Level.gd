extends Control


var Icon = load("res://scene/Icon.tscn")
var props = [
	{
		"ic_w": 68,
		"ic_h": 68,
		"gap_w": 3.5,
		"gap_h": 3.5,
		"offset_y": -4
	},
	{
		"ic_w": 68,
		"ic_h": 68,
		"gap_w": 3.5,
		"gap_h": 3.5,
		"offset_y": 0
	},
	{
		"ic_w": 68,
		"ic_h": 68,
		"gap_w": 3.5,
		"gap_h": 3.5,
		"offset_y": -4
	}
]
var bgs = [
	preload("res://assets/bg_1.png"),
	preload("res://assets/bg_2.png"),
	preload("res://assets/bg_3.png")
]
var boards = [
	preload("res://assets/board_1.png"),
	preload("res://assets/board_2.png"),
	preload("res://assets/board_3.png")
]
var titles = [
	preload("res://assets/title_lvl_1.png"),
	preload("res://assets/title_lvl_2.png"),
	preload("res://assets/title_lvl_3.png")
]
var icons = [
	preload("res://assets/icons/ic_1.png"),
	preload("res://assets/icons/ic_2.png"),
	preload("res://assets/icons/ic_3.png"),
	preload("res://assets/icons/ic_4.png"),
	preload("res://assets/icons/ic_5.png"),
	preload("res://assets/icons/ic_6.png"),
	preload("res://assets/icons/ic_7.png"),
	preload("res://assets/icons/ic_8.png"),
	preload("res://assets/icons/ic_9.png"),
	preload("res://assets/icons/ic_10.png")
]

var patterns = [
	[
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1]
	],
	[
		[0,1,1,1,0],
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1],
		[0,1,1,1,0]
	],
	[
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1],
		[1,1,1,1,1]
	]
]

var level = 1
var active_ic:Button = null
var sublings = []
var last_coords = [] # координаты удаляемых иконок


signal close_level


# сортировка для массива похожих иконок
func _my_custom_sort(a, b) -> bool:
	if a.x <= b.x and a.y <= b.y:
		return true
	else:
		return false


func _is_has_icon(x: int, y: int) -> bool:
	var ics = get_tree().get_nodes_in_group("icon_group")
	if ics.empty():
		return false
	for i in ics:
		if i.x == x and i.y == y:
			return true
	return false


# создание иконки
func create_icon() -> void:
	var L = level - 1
	# считаем всякие размеры и координаты
	var ic_w = props[L].ic_w
	var ic_h = props[L].ic_h
	var gap_w = props[L].gap_w
	var gap_h = props[L].gap_h
	var ic_count_x = patterns[L][0].size()
	var ic_count_y = patterns[L].size()
	var ics_w = ic_w * ic_count_x + gap_w * (ic_count_x - 1)
	var ics_h = ic_h * ic_count_y + gap_h * (ic_count_y - 1)

	# расставляем новые иконки
	var y = 0 # индекс по вертикали
	for rows in patterns[L]:
		var x = 0 # индекс по горизонтали
		for item in rows:
			if item == 0 or _is_has_icon(x, y):
				x += 1
				continue
			else:
				var icon = Icon.instance()
				icon.connect("icon_selected", self, "_on_Icon_icon_selected")
				icon.type = rand_range(0, icons.size())
				icon.x = x
				icon.y = y
				icon.get_node("Sprite").texture = icons[icon.type]
				icon.rect_position.x = ic_w * x + gap_w * x + ($Board.rect_size.x - ics_w) / 2
				icon.rect_position.y = ic_h * y + gap_h * y + ($Board.rect_size.y - ics_h) / 2 + props[L].offset_y
				$Board.add_child(icon)
				x += 1
		y += 1
	get_tree().get_nodes_in_group("icon_group").sort_custom(self, "_my_custom_sort")



# установка текстур уровня и расстановка иконок на доске по паттерну
func load_level(lvl: int) -> void:
	level = lvl
	var L = lvl - 1
	# сначала чистим старые иконки
	for i in get_tree().get_nodes_in_group("icon_group"):
		i.free()
	# расставляем текстуры
	$BG.texture = bgs[L]
	$Board.texture = boards[L]
	$Board.rect_size = Vector2.ZERO # HACK: чтобы сбрасывался размер доски
	$Title.texture = titles[L]
	# расставляем новые иконки
	create_icon()


# подсвечиваем иконки в окрестности фон Неймана первого порядка
# 0 1 0
# 1 @ 1
# 0 1 0
func find_sublings(x: int, y: int) -> void:
	for i in get_tree().get_nodes_in_group("icon_group"):
		if i.x == x and i.y == y-1 or i.x == x-1 and i.y == y or i.x == x+1 and i.y == y or i.x == x and i.y == y+1:
			sublings.append(i) # сохраняем соседние иконки в массив
			if not i.get_is_subling(): i.set_is_subling(true) # включаем подсветку


# перезаполнение доски в пустых клетках
func refill_board() -> void:
	var L = level - 1
	var ics = get_tree().get_nodes_in_group("icon_group")
	var pos_offset = props[L].ic_h + props[L].gap_h # сдвиг
	# двигаем столбцы до в пустые клетки
	last_coords.sort_custom(self, "_my_custom_sort")
	for i in last_coords:
		for j in ics:
			if i.x == j.x and i.y > j.y:
				j.go_to(j.rect_position + Vector2(0.0, pos_offset), j.x, j.y + 1)
	last_coords.clear()
	# расставляем новые иконки
	yield(get_tree().create_timer(0.1), "timeout")
	create_icon()



# сканирование доски на наличие комбинаций
func scan_board() -> void:
	var L = level - 1
	var similarities = []

	var grid = {
		"x": [],
		"y": []
	}
	# создаем массивы для горизонтальных и вертикальных линий доски
	for i in patterns[L]:
		grid.x.append([])
	for i in patterns[L][0]:
		grid.y.append([])

	# забиваем массивы иконками и отключаем подсветку соседей
	for i in get_tree().get_nodes_in_group("icon_group"):
		grid.x[i.y].append(i)
		grid.y[i.x].append(i)
		i.set_is_subling(false)

	# ищем в массивах минимум по три одинаковых иконки и добавляем их в единый массив
	for i in grid.x: # каждый массив в горизонтальных линиях
		i.sort_custom(self, "_my_custom_sort")
		for j in i.size(): # смотрим по количеству иконок в линии
			if j + 2 < i.size(): # если индекс + 2 находится в массиве
				if i[j].type == i[j + 1].type and i[j + 1].type == i[j + 2].type: # если совпадают типы у трех иконок
					for k in range(j, j + 3): # добавляем три иконки (текущий индекс и две следующих)
						# проверяем на дубликат в массиве и добавляем, если еще нет такой иконки
						if not similarities.has(i[k]):
							similarities.append(i[k])
							last_coords.append({
								"x": i[k].x,
								"y": i[k].y
							})
	for i in grid.y:
		i.sort_custom(self, "_my_custom_sort")
		for j in i.size():
			if j + 2 < i.size():
				if i[j].type == i[j + 1].type and i[j + 1].type == i[j + 2].type:
					for k in range(j, j + 3):
						if not similarities.has(i[k]):
							similarities.append(i[k])
							last_coords.append({
								"x": i[k].x,
								"y": i[k].y
							})
	yield(get_tree().create_timer(0.1), "timeout")
	if similarities.size() > 0:
		for i in similarities:
			i.death()
		# чистим массив похожих
		yield(get_tree().create_timer(0.3), "timeout")
		similarities.clear()
		get_tree().get_nodes_in_group("icon_group").sort_custom(self, "_my_custom_sort")
		refill_board()
		yield(get_tree().create_timer(0.3), "timeout")
		scan_board()






# func _ready() -> void:




func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if $Close.pressed:
			emit_signal("close_level")



# сигнал из иконки при клике
func _on_Icon_icon_selected(icon: Button) -> void:
	# если нет активной иконки
	if active_ic == null:
		active_ic = icon # прописываем активную иконку
		find_sublings(icon.x, icon.y) # подсвечиваем вокруг нее возможные ходы
	# если есть активная иконка
	else:
		# убираем подсветку с соседей
		for i in sublings:
			if i.get_is_subling():
				i.set_is_subling(false)

		# если кликали в активную иконку
		if icon == active_ic:
			active_ic = null
			sublings.clear() # чистим соседей из массива
		# если кликали вообще в другие иконки (не активная и не соседи)
		elif not sublings.has(icon):
			# снимаем выделение с текущей активной иконки
			if active_ic.get_is_active():
				active_ic.set_is_active(false)
			sublings.clear() # чистим соседей из массива
			# подсвечиваем новую иконку
			if not icon.get_is_active():
				icon.set_is_active(true)
			active_ic = icon # переписываем текущу активную иконку
			find_sublings(icon.x, icon.y) # и добавляем новых
		# если кликнули в соседа
		else:
			# снимаем выделение с текущей активной иконки
			if active_ic.get_is_active():
				active_ic.set_is_active(false)
			# снимаем выделение с выбранной соседней иконки
			if icon.get_is_active():
				icon.set_is_active(false)
			var active_ic_pos: Vector2 = active_ic.rect_position # сохраняем позицию активной иконки
			# сохраняем координаты активной иконки
			var active_ic_x: int = active_ic.x
			var active_ic_y: int = active_ic.y
			# передвигаем активную иконку на место выбранной соседней и меняем координаты
			active_ic.go_to(icon.rect_position, icon.x, icon.y) # active to subling position
			# передвигаем выбранную соседнюю иконку на место активной
			icon.go_to(active_ic_pos, active_ic_x, active_ic_y) # subling to active position
			sublings.clear() # чистим соседей из массива
			active_ic = null # чистим активную иконку
			scan_board() # сканируем доску на паттерны
