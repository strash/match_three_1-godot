extends Control


onready var pos = {
	"bg":               $BG.rect_position.x,
	"logo":             $Logo.rect_position.x,
	"start_btn":        $Start.rect_position.x,
	"level_btn":        $Level.rect_position.x,
	"title":            $Title.rect_position.x,
	"paging":           $Paging.rect_position.x,
	"levels_container": $LevelsContainer.rect_position.x,
	"back":             $Back.rect_position.x,
	"left":             $Left.rect_position.x,
	"right":            $Right.rect_position.x
}
var level = 1


signal set_level


# показываем экран уровней
func show_levels() -> void:
	var width = get_viewport_rect().size.x # viewport width
	var quarter = width / 4 # background offset
	$AnimationPlayer.stop() # нужно отключить анимацию, чтобы логотип отъехал

	# main menu
	$Tween.interpolate_property($BG, "rect_position:x", pos.bg, pos.bg - quarter, 0.8, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.interpolate_property($Logo, "rect_position:x", $Logo.rect_position.x, $Logo.rect_position.x - width, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT,0.2)
	$Tween.interpolate_property($Start, "rect_position:x", pos.start_btn, pos.start_btn - width, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, 0.1)
	$Tween.interpolate_property($Level, "rect_position:x", pos.level_btn, pos.level_btn - width, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	# level menu
	$Tween.interpolate_property($Back, "rect_position:x", $Back.rect_position.x, pos.back, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, 0.2)
	$Tween.interpolate_property($Title, "rect_position:x", $Title.rect_position.x, pos.title, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, 0.3)
	$Tween.interpolate_property($Paging, "rect_position:x", $Paging.rect_position.x, pos.paging, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, 0.4)
	$Tween.interpolate_property($LevelsContainer, "rect_position:x", $LevelsContainer.rect_position.x, pos.levels_container, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, 0.5)
	if not $Tween.is_active():
		$Tween.start()


# прячем экран уровней
func hide_levels() -> void:
	var width = get_viewport_rect().size.x # viewport width

	# level menu
	$Tween.interpolate_property($Back, "rect_position:x", $Back.rect_position.x, pos.back + width, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, 0.1)
	$Tween.interpolate_property($Title, "rect_position:x", $Title.rect_position.x, pos.title + width, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.interpolate_property($Paging, "rect_position:x", $Paging.rect_position.x, pos.paging + width, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, 0.1)
	$Tween.interpolate_property($LevelsContainer, "rect_position:x", $LevelsContainer.rect_position.x, pos.levels_container + width, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, 0.2)
	# main menu
	$Tween.interpolate_property($BG, "rect_position:x", $BG.rect_position.x, pos.bg, 0.8, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.interpolate_property($Logo, "rect_position:x", $Logo.rect_position.x, $Logo.rect_position.x + width, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT,0.3)
	$Tween.interpolate_property($Start, "rect_position:x", $Start.rect_position.x, pos.start_btn, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, 0.4)
	$Tween.interpolate_property($Level, "rect_position:x", $Level.rect_position.x, pos.level_btn, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, 0.5)
	if not $Tween.is_active():
		$Tween.start()

	$AnimateLogo.start() # запуск таймера
	level = 1


# отображение стрелок пагинации
func show_hide_paging_arrows(page: int) -> void:
	var offset = 100
	var left_pos: int
	var right_pos: int
	if page > 1:
		left_pos = pos.left + offset
	else:
		left_pos = pos.left
	if page < 3:
		right_pos = pos.right - offset
	else:
		right_pos = pos.right
	$Tween.interpolate_property($Left, "rect_position:x", $Left.rect_position.x, left_pos, 0.6, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.interpolate_property($Right, "rect_position:x", $Right.rect_position.x, right_pos, 0.6, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	if not $Tween.is_active():
		$Tween.start()


# установка страницы пагинации
func set_page(page: int) -> void:
	var i = 1
	var solid = Color(1.0, 1.0, 1.0, 1.0)
	var transparent = Color(1.0, 1.0, 1.0, 0.5)

	for node in $Paging.get_children():
		if i == page:
			$Tween.interpolate_property(node, "modulate", node.modulate, solid, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
		else:
			$Tween.interpolate_property(node, "modulate", node.modulate, transparent, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
		i += 1
	if not $Tween.is_active():
		$Tween.start()


# установка страницы уровня
func set_level(page: int) -> void:
	var sep = $LevelsContainer.get("custom_constants/separation")
	var block_w = ($LevelsContainer.rect_size.x - sep * 2) / 3
	var w = get_viewport_rect().size.x
	var new_pos = (w - block_w) / 2 - ((block_w + sep) * (page - 1))
	var new_bg_pos = -(w / 4 * (page + 1))
	$Tween.interpolate_property($LevelsContainer, "rect_position:x", $LevelsContainer.rect_position.x, new_pos, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.interpolate_property($BG, "rect_position:x", $BG.rect_position.x, new_bg_pos, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	if not $Tween.is_active():
		$Tween.start()


func _ready() -> void:
	# прячем экран уровней при загрузке игры
	var width = get_viewport_rect().size.x
	$Title.rect_position.x = pos.title + width
	$Paging.rect_position.x = pos.paging + width
	$LevelsContainer.rect_position.x = pos.levels_container + width
	$Back.rect_position.x = pos.back + width
	set_page(level)


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if $Start.pressed:
			emit_signal("set_level", level)
		if $Level.pressed:
			show_levels()
			show_hide_paging_arrows(level)
		if $Back.pressed:
			hide_levels()
			show_hide_paging_arrows(level)
			$Tween.interpolate_property($Right, "rect_position:x", $Right.rect_position.x, pos.right, 0.6, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
			if not $Tween.is_active():
				$Tween.start()
		if $Left.pressed:
			level -= 1
			if level < 1:
				level = 1
			set_page(level)
			set_level(level)
			show_hide_paging_arrows(level)
		if $Right.pressed:
			level += 1
			if level > 3:
				level = 3
			set_page(level)
			set_level(level)
			show_hide_paging_arrows(level)
		if $LevelsContainer/BtnLVL1.pressed:
			emit_signal("set_level", 1)
		if $LevelsContainer/BtnLVL2.pressed:
			emit_signal("set_level", 2)
		if $LevelsContainer/BtnLVL3.pressed:
			emit_signal("set_level", 3)


func _on_AnimateLogo_timeout() -> void:
	$AnimationPlayer.play("logo_idle")
